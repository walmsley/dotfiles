# fzf keybindings
if [ -f /usr/share/doc/fzf/examples/key-bindings.bash ]; then
    source /usr/share/doc/fzf/examples/key-bindings.bash
fi

# my bash aliases
alias update='sudo apt update;
    sudo apt upgrade;
    sudo apt autoremove --purge;
    sudo apt autoclean;
    tput setaf 6;
    figlet -ct -f kmono Complete'
# cargo install-update -a
# flatpak update;
# sudo snap refresh;

# alias cyb='chafa -s 80x80 -c 16 ~/Pictures/cyb/$(ls ~/Pictures/cyb | shuf -n 1)'
alias ..="cd .."
alias keyspd='xset r rate 300 50'
alias vertmon='xrandr --output DP-3 --rotate left --above eDP-1 && feh --bg-fill --no-fehbg ~/Pictures/Wallpapers/juliabg.png ~/Pictures/Wallpapers/juliavertbg.png'
alias slock='slock -m "$(cat ~/Desktop/slock/message.txt)"'
alias s=startx
alias v='nvim'
alias vi='nvim'
alias vim='nvim'
alias la='ls -A'
alias clang-format="clang-format -style=file:/home/kalankaboom/.config/.clang-format"
alias pymol='python3 -m pymol -x'
alias bc='bc ~/.bcrc -ql'
alias metro='~/.custom/./metro.sh'
alias :q='exit'
# alias venv-activate='source ./bin/activate && PS1="\[\033[01;35m\]┌─── \[\033[01;36m\]\w \[\033[0;33m\](${VIRTUAL_ENV##*/})\n\[\033[01;35m\]└ \[\033[00m\]"'
alias cbonsai='cbonsai -lit .5'
alias nyancat="~/Desktop/nyancat/src/./nyancat"
alias ncm="ncmpcpp -q"
alias zat='zathura --fork'
alias cyb='chafa --scale max --symbols braille -c 16 ~/Pictures/cyb/$(ls ~/Pictures/cyb | shuf -n 1)'
alias scrot="scrot ~/Pictures/Screenshots/%Y-%m-%d-%H-%M-%S.png -e 'xclip -selection clipboard -t image/png -i ~/Pictures/Screenshots/%Y-%m-%d-%H-%M-%S.png'"
alias mnt="udisksctl mount -b"
alias figlet='figlet -f kmono'
alias cmatrix='cmatrix -bC cyan'
alias stmpd="systemctl start --user mpd"
alias stpa="systemctl start --user pulseaudio"
alias jmp='. /usr/share/autojump/autojump.sh'
alias vpnepfl='sudo openconnect -b vpn.epfl.ch'

alias nmgui="nm-applet    2>&1 > /dev/null &
    stalonetray  2>&1 > /dev/null
    killall nm-applet"

function venv-activate() {
    input_dir="$1"

    if [ -d "$input_dir" ]; then
        VIRTUAL_ENV_DISABLE_PROMPT=1
        source $input_dir"/bin/activate"
    else
        VENV_PATH=~/.myvenvs/
        ret=$(ls $VENV_PATH | dmenu -i)
        if [[ "$ret" == "" ]]; then
            return
        fi
        VIRTUAL_ENV_DISABLE_PROMPT=1
        source $VENV_PATH$ret"/bin/activate"
    fi
    # PS1="\[\033[01;35m\]┌─── \[\033[01;36m\]\w \[\033[0;33m\](${VIRTUAL_ENV##*/})\n\[\033[01;35m\]└ \[\033[00m\]"
}

function clock() {
    str=$(curl -s wttr.in/?format="%l:+%C+%t+%p\n")
    tput civis
    watch -ct -n 1 "echo -n '\033[36;1m';
    date '+%T'|figlet -ct -f kmono;
    echo -n '\033[37;0m';
    date '+%A%t%d/%m/%Y'|figlet -ct -f future;
    echo '\033[37;2m';
    figlet -ct -f term $str;"
    tput cnorm
}

function note-upload() {
    cd ~/Documents/notes/
    git add .
    printf "\n"
    git commit -m "$(date +'%d/%m/%Y')"
    printf "\n"
    git push -u origin main
    # sudo ssmtp kalankaboom@murena.io < email.txt;
}

alias note-pull='cd ~/Documents/Notes/; git pull "git@gitlab.epfl.ch:walmsley/sv-ba2.git";'

function note() {
    FILE_TYPE=$(echo $1 | grep -E -o '\..+')
    FILE_DIR=~/Documents/Notes/$(echo $FILE_TYPE | grep -E -o '[^\.]+')
    FILE_PATH=$FILE_DIR/$1
    if [ -n "$(echo $1 | grep -o '/')" ]; then
        FILE_DIR=$FILE_DIR/$(echo $1 | grep -P -o '.+(?=/.+\.)')
    fi
    if [ ! -d "$FILE_DIR" ]; then
        mkdir -p $FILE_DIR
    fi
    TEMP=$(find ~/Templates/Quickplates/ -type f -name "*$FILE_TYPE")
    if [ -f "$FILE_PATH" ]; then
        echo -e "\e[95;1mFile already exists!\e[0m"
    else
        cp $TEMP $FILE_PATH
        if [ "$FILE_TYPE" = ".tex" ] || [ "$FILE_TYPE" = ".md" ]; then
            nvim -c "%s/\[today\]/\=strftime('%d-%m-%Y')" -c "%s/\[title\]/\=expand('%:t:r')" $FILE_PATH
        else
            nvim $FILE_PATH
        fi
    fi
}

function stmamba() {
    export MAMBA_EXE='/home/kalankaboom/.local/bin/micromamba'
    export MAMBA_ROOT_PREFIX='/home/kalankaboom/.micromamba'
    __mamba_setup="$("$MAMBA_EXE" shell hook --shell bash --root-prefix "$MAMBA_ROOT_PREFIX" 2>/dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__mamba_setup"
    else
        alias micromamba="$MAMBA_EXE" # Fallback on help from mamba activate
    fi
    unset __mamba_setup
}
