cd ~
while
	ret=$(ls | dmenu -i)
	if [[ "$ret" == "" ]]; then
		exit
	fi
	cd "$ret"
	succ=$(echo "$?")
	[[ $succ != 1 ]]
do true; done
export TERMINAL="st" && xdg-open "$ret" &
