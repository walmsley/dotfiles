#!/bin/bash

# Set BPM and beats per measure.
bpm="${1-60}"
msr="${2-4}"

# Get seconds per beat using bc.
beat_time="$(bc -l <<<"scale=5; 60/$bpm")"

echo ""

while true; do
    for ((i = 1; i <= $msr; i++)); do
        if [[ $i -eq 1 ]]; then
            # Accentuated beat.
            mpv --no-terminal ~/Music/TICK.ogg &
        else
            # Unaccentuated beat
            mpv --no-terminal ~/Music/tick.ogg &
        fi

        printf "\033[1A" # move cursor one line up
        echo -ne "\r [ $bpm BPM - $msr beats/measure ]  \033[0;36m├"
        for ((j = 1; j <= $msr; j++)); do
            if [[ $j -eq $i ]]; then
                if [[ $i -eq 1 ]]; then
                    echo -ne "\033[0;95m"
                else
                    echo -ne "\033[0;35m"
                fi
                echo -n ""
            else
                echo -ne "\033[0;36m─"
            fi
        done
        echo -e "\033[0;36m┤\033[0m"
        sleep "$beat_time" || exit
    done
done
