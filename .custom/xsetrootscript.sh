#!/bin/sh

batt=$(cat /sys/class/power_supply/BAT1/capacity)
if [ "$(cat /sys/class/power_supply/BAT1/status)" = "Charging" ]; then
    if [ $batt -lt 100 ]; then
        if [ $batt -eq 80 ]; then
            herbe "Power High"
        fi
        battstring="󰂄 $batt% "
    else
        battstring="󰂄 100%"
    fi
else
    if [ $batt -lt 10 ]; then
        if [ $batt -eq 3 ]; then
            systemctl poweroff
        fi
        battstring="󰂎 $batt%  "
        herbe "Power Critical" &
        ~/.custom/./powermenu.sh
    elif [ $batt -lt 20 ]; then
        battstring="󰁺 $batt% "
    elif [ $batt -lt 30 ]; then
        battstring="󰁻 $batt% "
        if [ $batt -eq 20 ]; then
            herbe "Power Low" &
            ~/.custom/./powermenu.sh
        fi
    elif [ $batt -lt 40 ]; then
        battstring="󰁼 $batt% "
    elif [ $batt -lt 50 ]; then
        battstring="󰁽 $batt% "
    elif [ $batt -lt 60 ]; then
        battstring="󰁾 $batt% "
    elif [ $batt -lt 70 ]; then
        battstring="󰁿 $batt% "
    elif [ $batt -lt 80 ]; then
        battstring="󰂀 $batt% "
    elif [ $batt -lt 90 ]; then
        battstring="󰂁 $batt% "
    elif [ $batt -lt 100 ]; then
        battstring="󰂂 $batt% "
    else
        battstring="󰁹 100%"
    fi
fi

brightness=$(brightnessctl -m | grep -Po '(?<=,)[^,]*(?=%)')
if [ $brightness -lt 10 ]; then
    brstring="󰃞 $brightness%  "
elif [ $brightness -lt 100 ]; then
    brstring="󰃟 $brightness% "
else
    brstring="󰃠 100%"
fi

mute=$(amixer get Master | grep -Po -m 1 "(?<=\[)[ofn]*(?=\])")
vol=$(amixer get Master | grep -Po -m 1 "[0-9]*(?=%)")

if [ "$mute" = "off" ]; then
    if [ $vol -lt 10 ]; then
        volstring="󱟪 $vol%  "
    elif [ $vol -lt 100 ]; then
        volstring="󱟪 $vol% "
    else
        volstring="󱟪 100%"
    fi
else
    if [ $vol -lt 10 ]; then
        volstring="󰞴 $vol%  "
    elif [ $vol -lt 34 ]; then
        volstring="󰞴 $vol% "
    elif [ $vol -lt 67 ]; then
        volstring="󰞳 $vol% "
    elif [ $vol -lt 100 ]; then
        volstring="󰞲 $vol% "
    else
        volstring="󰞲 100%"
    fi
fi

bluetooth=$(hcitool dev | grep -o "hci")
if [ "$bluetooth" = "" ]; then
    btstring="󰂲"
else
    btdev=$(bluetoothctl devices Connected)
    if [ "$btdev" = "" ]; then
        btstring="󰂯"
    else
        btstring="󰂱"
    fi
fi

ether=$(nmcli con show --active | grep -o "ethernet")
if [ "$ether" = "" ]; then
    wifi=$(awk 'NR==3 {printf("%.0f",$3*10/7)}' /proc/net/wireless)
    if [ "$wifi" = "" ]; then
        wifistring="󰖪     "
    else
        if [ $wifi -lt 100 ]; then
            wifistring="󰖩 $wifi% "
        else
            wifistring="󰖩 100%"
        fi
    fi
else
    wifistring="󰈀     "
fi

minutes=$(printf '%02d' "$(expr $(date '+%M') + 1)")
hours=$(date "+%H")
if [ $minutes = 60 ]; then
    minutes=00
    hours=$(printf '%02d' "$(expr $hours + 1)")
    if [ $hours = 25 ]; then
        hours="00"
    fi
fi
xsetroot -name " $wifistring  $btstring $volstring  $brstring  $battstring  $hours:$minutes "
