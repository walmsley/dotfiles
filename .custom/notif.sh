#!/bin/sh

file="$HOME/.notifs"

tiramisu -j |
    while read -r notif; do

        _=${notif#*\"source\": \"}
        source=${_%%\",*}
        source=$(echo "$source" | tr [:lower:] [:upper:])

        _=${notif#*\"summary\": \"}
        summary="${_%%\",*}"

        _=${notif#*\"body\": \"}
        body="${_%%\",*}"

        disp=$(printf "$source\n\n$summary\n\n$body")

        echo "$disp\n\n---\n" >>"$file"
        herbe "$disp" &
    done
