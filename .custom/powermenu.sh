#!/bin/bash

readonly ignore=" Ignore"
readonly shutdown=" Shutdown"
readonly suspend="󰤄 Suspend"
readonly hibernate=" Hibernate"
readonly reboot=" Reboot"

readonly power=$(
	echo -e "$ignore\n$shutdown\n$suspend\n$hibernate\n$reboot" | dmenu
)

echo "$power"

if [ "$power" = "" ] || [ "$power" = "$ignore" ]; then
	exit
fi

readonly confirm="${power:0:1} Confirm $(
	echo ${power##* } | tr '[:upper:]' '[:lower:]'
)"
readonly cancelm="󰚌 Cancel"

readonly confirmation=$(
	echo -e "$confirm\n$cancelm" | dmenu
)

if [ "$confirmation" = "" ] || [ "$confirmation" = "$cancelm" ]; then
	exit
fi

if [ "$power" = "$shutdown" ]; then
	systemctl poweroff
elif [ "$power" = "$suspend" ]; then
	systemctl suspend
elif [ "$power" = "$hibernate" ]; then
	systemctl hibernate
elif [ "$power" = "$reboot" ]; then
	systemctl reboot
fi
