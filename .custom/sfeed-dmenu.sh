#!/bin/sh
url=$(sfeed_plain "$HOME/.sfeed/feeds/"* | dmenu -l 35 -i | \
	sed -n 's@^.* \([a-zA-Z]*://\)\(.*\)$@\1\2@p')
if [ "${url}" != "" ]; then
	testurl=$(echo ${url} | grep -o 'https://www\.youtube\.com')
	if [ "${testurl}" = "" ]; then
		firefox "${url}"
	else
		mpv "${url}"
	fi
fi
# test -n "${url}" && firefox "${url}"
