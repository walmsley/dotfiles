# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
    *) return ;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
# if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
#     debian_chroot=$(cat /etc/debian_chroot)
# fi

# set a fancy prompt (non-color, unless we know we "want" color)
# case "$TERM" in
#     xterm-color|*-256color) color_prompt=yes;;
# esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

function updateps1() {
    sep="\[\033[1;35m\] • \[\033[0;36m\]"
    additions=""

    gitps1="$(__git_ps1)"
    if [ -n "$gitps1" ]; then
        additions+=$sep${gitps1:2:${#gitps1}-3}
    fi

    if [ -n "$CONDA_DEFAULT_ENV" ]; then
        additions+=$sep$CONDA_DEFAULT_ENV
    fi

    if [ -n "$VIRTUAL_ENV" ]; then
        additions+=$sep${VIRTUAL_ENV##*/}
    fi

    PS1='\[\033[1;35m\]┌─── \[\033[1;36m\]\w'$additions'\n\[\033[1;35m\]└ \[\033[0m\]'
}

PS1='\[\033[1;35m\]┌─── \[\033[1;36m\]\w\n\[\033[1;35m\]└ \[\033[0m\]'

PROMPT_COMMAND=updateps1

# if [ "$color_prompt" = yes ]; then
# #    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;36m\]\u@\h\[\033[00m\]:\[\033[01;35m\]\w\[\033[00m\]\$ '
# #    PS1='\[\033[01;36m\] \w\[\033[00m\] Ξ› '
#     PS1='\[\033[01;35m\]┌─── \[\033[01;36m\]\w\n\[\033[01;35m\]└ \[\033[00m\]'
#
# else
#     PS1='┌─── \w\n└ '
#     # PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
# fi

unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
# case "$TERM" in
# xterm*|rxvt*)
#     PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
#     ;;
# *)
#     ;;
# esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    #alias fgrep='fgrep --color=auto'
    #alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
#alias ll='ls -l'
#alias la='ls -A'
#alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

# if [ -f ~/.bash_aliases ]; then
. ~/.bash_aliases
# fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

export COLORTERM="truecolor"

export EDITOR="nvim"
export VISUAL="nvim"

if [ -f ~/Desktop/nnn/misc/quitcd/quitcd.bash_sh_zsh ]; then
    source ~/Desktop/nnn/misc/quitcd/quitcd.bash_sh_zsh
fi

# tty colours
if [ "$TERM" = "linux" ]; then
    # tty cursor
    echo -en "\033[?112c"

    echo -en "\e]P0000000" #black
    echo -en "\e]P8555555" #darkgrey
    echo -en "\e]P1990000" #darkred
    echo -en "\e]P9FF0000" #red
    echo -en "\e]P2009900" #darkgreen
    echo -en "\e]PA00FF00" #green
    echo -en "\e]P3999900" #brown
    echo -en "\e]PBFFFF00" #yellow
    echo -en "\e]P40000AA" #darkblue
    echo -en "\e]PC5555FF" #blue
    echo -en "\e]P5990099" #darkmagenta
    echo -en "\e]PDFF00FF" #magenta
    echo -en "\e]P6009999" #darkcyan
    echo -en "\e]PE00FFFF" #cyan
    echo -en "\e]P7999999" #lightgrey
    echo -en "\e]PFFFFFFF" #white
    clear                  #for background artifacting
fi

export FZF_DEFAULT_OPTS='--color bg+:5,border:15,spinner:14,hl:6,prompt:13,info:12,pointer:14,marker:11,fg+:15,hl+:6,gutter:-1'

export NNN_COLORS="5362"
BLK="0B" CHR="0B" DIR="06" EXE="06" REG="05" HARDLINK="06" SYMLINK="06" MISSING="08" ORPHAN="09" FIFO="06" SOCK="0B" OTHER="06"
export NNN_FCOLORS="$BLK$CHR$DIR$EXE$REG$HARDLINK$SYMLINK$MISSING$ORPHAN$FIFO$SOCK$OTHER"
export NNN_TRASH=1
export NNN_BMS="p:$HOME/Documents/Notes/pdf;t:$HOME/Documents/Notes/tex;"

export TERMINAL='st'

# export LIBVA_DRIVER_NAME=i965
# export VDPAU_DRIVER=va_gl

export PATH=$PATH:~/.cargo/bin

export PATH=$PATH:~/go/bin

# [ -z "$TMUX" ] && [ "$TERM" = "st-256color" ] && { tmux attach-session -t 󰛸 || exec tmux new-session -s 󰛸 && exit; }

# WARNING
# WARNING
# export PATH=$PATH:~/Desktop/emsdk
# export PATH=$PATH:~/Desktop/emsdk/upstream/emscripten
# WARNING
# WARNING
