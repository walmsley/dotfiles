local capabilities = require("cmp_nvim_lsp").default_capabilities()
local cmp = require("cmp")
local colours = require("kalankaboom.colours")
local dict = require("cmp_dictionary")
local formatter = require("formatter")
local lspconfig = require("lspconfig")
local lualine = require("lualine")
local luasnip = require("luasnip")
local mason = require("mason")
local mason_lspconfig = require("mason-lspconfig")
local nnn = require("nnn")
local npairs = require("nvim-autopairs")
local rule = require("nvim-autopairs.rule")
local treesitter_configs = require("nvim-treesitter.configs")
local treesitter_context = require("treesitter-context")
local trouble = require("trouble")
local util = require("formatter.util")
require("Comment").setup()
require("kalankaboom").setup()
require("luasnip.loaders.from_vscode").lazy_load()
require("nvim-autopairs.completion.cmp")
require("nvim-ts-autotag").setup()

--[[
-- Options
--]]
vim.opt.title = true
vim.opt.titlestring = "%t"
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.undofile = true
vim.opt.wrap = false

local glob_shift_width = 4 -- used below to always have same indent
vim.opt.ts = 2
vim.opt.sw = 4
vim.opt.sts = 4
vim.opt.smarttab = true
vim.opt.smartindent = true
vim.opt.expandtab = true

vim.opt.scrolloff = 3
vim.opt.splitright = true
vim.opt.splitbelow = true

vim.opt.signcolumn = "number"

vim.opt.updatetime = 50

-- set switchbuf=vsplit
-- set foldminlines=8

vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"

--[[
-- Autocmd
--]]
vim.api.nvim_create_autocmd("TermOpen", {
    pattern = "*",
    command = "setlocal nonumber norelativenumber",
})

-- vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
--     pattern = "*.md",
--     command = "set conceallevel=2",
-- })

--[[
-- Keymaps
--]]
vim.g.mapleader = " "

vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])
vim.keymap.set("x", "<leader>p", [["_dP]])
vim.keymap.set("n", "<leader>q", "<cmd>Format<CR>")
-- vim.keymap.set("n", "<leader>q", vim.lsp.buf.format)

vim.keymap.set("n", "<C-j>", "<C-e>")
vim.keymap.set("n", "<C-k>", "<C-y>")
vim.keymap.set("n", "<A-Space>", "<S-j>")
vim.keymap.set("n", "<S-j>", "<S-Down>")
vim.keymap.set("n", "<S-k>", "<S-Up>")
vim.keymap.set("n", "<C-l>", ":vsplit<CR> <C-w>r :NnnPicker<CR>")
vim.keymap.set("n", "<C-h>", ":split<CR> <C-w>r :NnnPicker<CR>")

vim.keymap.set("v", "<S-j>", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "<S-k>", ":m '<-2<CR>gv=gv")

vim.keymap.set("x", "<CR>", function()
    if vim.api.nvim_get_mode()["mode"] == "V" then
        return [["zy'<"zP:call setreg('z', [])<CR>gv]]
    else
        return "<CR>"
    end
end, { expr = true })
vim.keymap.set("x", "<A-CR>", function()
    if vim.api.nvim_get_mode()["mode"] == "V" then
        return [["zy'>"zp:call setreg('z', [])<CR>gv]]
    else
        return "<CR>"
    end
end, { expr = true })

vim.keymap.set("n", "n", "nzz")
vim.keymap.set("n", "N", "Nzz")

vim.keymap.set("t", "<leader>h", [[<C-\><C-N><C-w>h]])
vim.keymap.set("t", "<leader>j", [[<C-\><C-N><C-w>j"]])
vim.keymap.set("t", "<leader>k", [[<C-\><C-N><C-w>k"]])
vim.keymap.set("t", "<leader>l", [[<C-\><C-N><C-w>l"]])
vim.keymap.set("t", "<ESC>", [[<C-\><C-N>]])

vim.keymap.set("n", "<leader>h", "<C-w>h")
vim.keymap.set("n", "<leader>j", "<C-w>j")
vim.keymap.set("n", "<leader>k", "<C-w>k")
vim.keymap.set("n", "<leader>l", "<C-w>l")

vim.keymap.set("i", "<Tab>", function()
    if luasnip.expand_or_jumpable() then
        return "<Plug>luasnip-expand-or-jump"
    else
        return "<Tab>"
    end
end, { expr = true })
vim.keymap.set("i", "<S-Tab>", function() luasnip.jump(-1) end)
vim.keymap.set("s", "<Tab>", function() luasnip.jump(1) end)
vim.keymap.set("s", "<S-Tab>", function() luasnip.jump(-1) end)

vim.keymap.set("n", "<leader>d", "<cmd>TroubleToggle<CR>")

vim.keymap.set("i", "<C-c>", "<ESC>")
vim.keymap.set("i", "<C-[>", "<ESC>")
-- vim.keymap.set("i", "<A-j>", "j")
-- vim.keymap.set("i", "jj", "<ESC>")
-- vim.keymap.set("i", "jh", "<ESC>h")
-- vim.keymap.set("i", "jk", "<ESC>k")
-- vim.keymap.set("i", "jl", "<ESC>l")
-- vim.keymap.set("i", "ji", "<ESC>i")
-- vim.keymap.set("i", "ja", "<ESC>la")
-- vim.keymap.set("i", "jp", "<ESC>p")

vim.keymap.set("t", "<leader>t", "<cmd>NnnExplorer<CR>")
vim.keymap.set("n", "<leader>t", "<cmd>NnnExplorer %:p:h<CR>")
vim.keymap.set("t", "<leader>f", "<cmd>NnnPicker<CR>")
vim.keymap.set("n", "<leader>f", "<cmd>NnnPicker %:p:h<CR>")
vim.keymap.set("n", "<leader>u", "<cmd>UndotreeToggle<CR>")

vim.keymap.set("n", "<leader>xx", "<cmd>%!xxd<CR><cmd>set ft=xxd<CR>")
vim.keymap.set("n", "<leader>xw", "<cmd>%!xxd -r<CR><cmd>filetype detect<CR>")

vim.keymap.set("i", "<A-1>", "é")
vim.keymap.set("i", "<A-2>", "è")
vim.keymap.set("i", "<A-3>", "ê")
vim.keymap.set("i", "<A-4>", "à")
vim.keymap.set("i", "<A-5>", "ù")
vim.keymap.set("i", "<A-6>", "û")
vim.keymap.set("i", "<A-7>", "ç")
vim.keymap.set("i", "<A-8>", "ô")
vim.keymap.set("i", "<A-9>", "î")
vim.keymap.set("i", "<A-0>", "ï")

--[[
-- Autocomp
--]]
local function file_exists(name)
    local file = io.open(name, "r")
    if file ~= nil then
        io.close(file)
        return true
    end
    return false
end

local function start_comp_message()
    vim.cmd("echohl MoreMsg")
    vim.cmd("echo 'Compilation started!'")
    vim.cmd("echohl None")
end

local function end_comp_message()
    vim.cmd("echohl WarningMsg")
    vim.cmd("echo 'Compilation stopped!'")
    vim.cmd("echohl None")
end

local autocomp = true
function Auto_compile(compile_cmd, cleanup_cmd, view_cmd, view_without_comp)
    if autocomp then
        local autogroup =
            vim.api.nvim_create_augroup("autocomp", { clear = true })

        local jobstart_pre = "call jobstart('"
        local jobstart_post = "', {'detach':1})"

        vim.api.nvim_create_autocmd("BufWritePost", {
            pattern = "*",
            group = autogroup,
            command = jobstart_pre .. compile_cmd .. jobstart_post,
        })

        if cleanup_cmd ~= "" then
            vim.api.nvim_create_autocmd("QuitPre", {
                pattern = "*",
                group = autogroup,
                command = jobstart_pre .. cleanup_cmd .. jobstart_post,
            })
        end

        if view_cmd ~= "" and view_without_comp ~= nil then
            if view_without_comp then
                vim.cmd(jobstart_pre .. view_cmd .. jobstart_post)
            else
                vim.cmd(
                    jobstart_pre
                        .. compile_cmd
                        .. " && "
                        .. view_cmd
                        .. jobstart_post
                )
            end
        end
        autocomp = false
        start_comp_message()
    else
        vim.api.nvim_clear_autocmds({ group = "autocomp" })
        autocomp = true
        end_comp_message()
    end
end

function Auto_compile_tex()
    local latexmk_cmd = "latexmk -quiet -shell-escape -outdir="
        .. vim.fn.expand("%:p:h")
        .. " -pdf "
        .. vim.fn.expand("%:p")

    local path_and_name = vim.fn.expand("%:p:r")

    local tmp_file_ext = {
        ".bcf",
        ".thm",
        ".fls",
        ".out",
        ".aux",
        ".bbl",
        ".blg",
        ".fdb_latexmk",
        ".log",
        ".run.xml",
        ".toc",
    }

    for i = 1, #tmp_file_ext do
        tmp_file_ext[i] = path_and_name .. tmp_file_ext[i]
    end

    local cleanup_cmd = "rm " .. table.concat(tmp_file_ext, " ")

    local zathura_cmd = "zathura --fork " .. path_and_name .. ".pdf"

    local already_comped = file_exists(path_and_name .. ".pdf")

    Auto_compile(latexmk_cmd, cleanup_cmd, zathura_cmd, already_comped)
end

function Auto_compile_mark()
    local pandoc_cmd = "pandoc "
        .. vim.fn.expand("%:p")
        .. " $HOME/Templates/fancy-metadata.yaml --highlight-style $HOME/Templates/pantheme.theme -F mermaid-filter --number-sections --columns=1000 -s -o "
        .. vim.fn.expand("%:p:r")
        .. ".pdf"

    local path_and_name = vim.fn.expand("%:p:r")

    local zathura_cmd = "zathura --fork " .. path_and_name .. ".pdf"

    local already_comped = file_exists(path_and_name .. ".pdf")

    Auto_compile(pandoc_cmd, "", zathura_cmd, already_comped)
end

vim.api.nvim_create_autocmd("FileType", {
    pattern = "tex",
    command = [[nnoremap \ll <cmd>lua Auto_compile_tex()<CR>]],
})

vim.api.nvim_create_autocmd("FileType", {
    pattern = "markdown",
    command = [[nnoremap \ll <cmd>lua Auto_compile_mark()<CR>]],
})

function CleanSVGHTML()
    local cmd =
        [[exe "normal f\"gfVGy\<C-^>VpV:s/style=\"[^\"]*\" /\<CR>gv:s/<style>[^<]*<\\/style>/\<CR>"]]
    vim.cmd(cmd)
end

vim.api.nvim_create_autocmd("FileType", {
    pattern = "html",
    command = [[nnoremap \ll <cmd>g/<img src="\.\/[^\.]*.svg" alt="[^"]*" \/>/lua CleanSVGHTML()<CR>]],
})

--[[
-- Plugins
--]]

-- colorcolumn
local curcol = 0
vim.keymap.set("n", "<leader>o", function()
    curcol = math.abs(curcol - 81)
    return "<cmd>set colorcolumn=" .. curcol .. "<CR>"
end, { expr = true })

-- diagnostics
vim.diagnostic.config({
    virtual_text = false,
    severity_sort = true,
    signs = true,
    float = {
        border = "single",
        source = "always",
        header = "",
        prefix = "",
    },
})

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
    border = "single",
    title = "",
})

vim.lsp.handlers["textDocument/signatureHelp"] =
    vim.lsp.with(vim.lsp.handlers.signature_help, {
        border = "single",
    })

local signs = { Error = " ", Warn = " ", Hint = "󰌶 ", Info = " " }
for type, icon in pairs(signs) do
    local hl = "DiagnosticSign" .. type
    vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

-- mode colour
vim.api.nvim_create_autocmd("InsertEnter", {
    pattern = "*",
    command = "highlight CursorLineNr guifg=" .. colours.Cyan,
})
vim.api.nvim_create_autocmd("InsertLeave", {
    pattern = "*",
    command = "highlight CursorLineNr guifg=" .. colours.DarkCyan,
})
vim.api.nvim_create_autocmd("ModeChanged", {
    pattern = "[vV\x16]*:*",
    command = "highlight CursorLineNr guifg=" .. colours.DarkCyan,
})
vim.api.nvim_create_autocmd("ModeChanged", {
    pattern = "*:*[vV\x16]",
    command = "highlight CursorLineNr guifg=" .. colours.Yellow,
})

-- mason
mason.setup({
    ui = {
        icons = {
            package_installed = "",
            package_pending = "",
            package_uninstalled = "",
        },
        border = "single",
    },
})

mason_lspconfig.setup()

-- on lsp-attach keybindings
local on_attach = function(_, bufnr)
    local opts = { buffer = bufnr, remap = false }

    vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
    vim.keymap.set("n", "<CR>", function() vim.lsp.buf.hover() end, opts)
    vim.keymap.set(
        "n",
        "<leader><CR>",
        function() vim.diagnostic.open_float() end,
        opts
    )
    vim.keymap.set(
        "n",
        "<leader>[",
        function() vim.diagnostic.goto_next() end,
        opts
    )
    vim.keymap.set(
        "n",
        "<leader>]",
        function() vim.diagnostic.goto_prev() end,
        opts
    )
    vim.keymap.set(
        "n",
        "<leader>a",
        function() vim.lsp.buf.code_action() end,
        opts
    )
    vim.keymap.set(
        "n",
        "<leader>rr",
        function() vim.lsp.buf.references() end,
        opts
    )
    vim.keymap.set("n", "<leader>rn", function() vim.lsp.buf.rename() end, opts)
    vim.keymap.set(
        "i",
        "<A-h>",
        function() vim.lsp.buf.signature_help() end,
        opts
    )
end

mason_lspconfig.setup_handlers({
    function(server_name)
        lspconfig[server_name].setup({
            capabilities = capabilities,
            on_attach = on_attach,
        })
    end,
    ["lua_ls"] = function()
        lspconfig.lua_ls.setup({
            capabilities = capabilities,
            on_attach = on_attach,
            settings = {
                Lua = {
                    diagnostics = {
                        globals = {
                            "vim",
                        },
                    },
                    workspace = {
                        --     library = vim.api.nvim_get_runtime_file("", true),
                        checkThirdParty = false,
                    },
                },
            },
        })
    end,
    ["texlab"] = function()
        lspconfig.texlab.setup({
            capabilities = capabilities,
            on_attach = on_attach,
            settings = {
                texlab = {
                    diagnostics = {
                        ignoredPatterns = {
                            'siunitx Warning: Detected the "physics"',
                        },
                    },
                },
            },
        })
    end,
})

-- nnn
nnn.setup({
    explorer = {
        width = 24,
        side = "topleft",
    },
    picker = {
        style = {
            width = 0.8,
            height = 0.8,
        },
    },
    replace_netrw = "explorer",
})

-- autopairs
npairs.setup()
npairs.add_rule(rule("\\(", "\\)", "tex"))
npairs.add_rule(rule("\\[", "\\]", "tex"))
npairs.add_rule(rule("\\left|", "\\right|", { "tex", "markdown" }))
npairs.add_rule(rule("\\left(", "\\right)", { "tex", "markdown" }))
npairs.add_rule(rule("\\left\\{", "\\right\\}", { "tex", "markdown" }))
npairs.add_rule(rule("\\left[", "\\right]", { "tex", "markdown" }))
npairs.add_rule(rule("\\left]", "\\right[", { "tex", "markdown" }))
npairs.add_rule(rule("$", "$", "markdown"))
npairs.get_rules("'")[1].not_filetypes = { "tex" }

-- treesitter
treesitter_configs.setup({
    ensure_installed = "all",
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
    },
    indent = { enable = true, disable = { "" } },
    context_commentstring = {
        enable = true,
    },
    playground = {
        enable = true,
        disable = {},
        updatetime = 25,
        persist_queries = false,
        keybindings = {
            toggle_query_editor = "o",
            toggle_hl_groups = "i",
            toggle_injected_languages = "t",
            toggle_anonymous_nodes = "a",
            toggle_language_display = "I",
            focus_language = "f",
            unfocus_language = "F",
            update = "R",
            goto_node = "<cr>",
            show_help = "?",
        },
    },
})

-- treesitter context
treesitter_context.setup({
    enable = true,
    max_lines = 0,
    min_window_height = 0,
    line_numbers = true,
    multiline_threshold = 3,
    trim_scope = "outer",
    mode = "cursor",
    separator = nil,
    zindex = 20,
    on_attach = nil,
})

-- cmp
local icons = {
    Class = "",
    Color = "󰏘",
    Constant = "",
    Constructor = "󰖷",
    Enum = "",
    EnumMember = "",
    Event = "",
    Field = "",
    File = "󰈔",
    Folder = "󰉋",
    Function = "󰊕",
    Interface = "",
    Keyword = "",
    Method = "",
    Module = "󰕳",
    Operator = "",
    Property = "",
    Reference = "",
    Snippet = "",
    Struct = "",
    Text = "󰉿",
    TypeParameter = "",
    Unit = "",
    Value = "󰎠",
    Variable = "󰏗",
}

cmp.setup({
    snippet = {
        expand = function(args) luasnip.lsp_expand(args.body) end,
    },
    formatting = {
        fields = { "kind", "abbr" },
        format = function(_, vim_item)
            vim_item.kind = string.format("%s", icons[vim_item.kind])
            return vim_item
        end,
    },
    window = {
        completion = cmp.config.window.bordered({
            border = "single",
            winhighlight = "Normal:Pmenu,FloatBorder:CmpBorder,CursorLine:PmenuSel,Search:None",
        }),
        documentation = cmp.config.window.bordered({
            border = "single",
            winhighlight = "FloatBorder:CmpBorder",
        }),
    },
    mapping = cmp.mapping.preset.insert({
        ["<C-p>"] = cmp.mapping.scroll_docs(-4),
        ["<C-n>"] = cmp.mapping.scroll_docs(4),
        ["<A-TAB>"] = cmp.mapping.abort(),
        ["<CR>"] = cmp.mapping.confirm({ select = false }),
        ["<Tab>"] = cmp.mapping.select_next_item(),
        ["<S-Tab>"] = cmp.mapping.select_prev_item(),
    }),
    sources = cmp.config.sources({
        { name = "nvim_lsp" },
        { name = "luasnip" },
        { name = "buffer" },
    }),
})

cmp.setup.filetype({ "markdown", "tex" }, {
    snippet = {
        expand = function(args) luasnip.lsp_expand(args.body) end,
    },
    sources = cmp.config.sources({
        { name = "nvim_lsp" },
        { name = "luasnip" },
        { name = "buffer" },
        {
            name = "dictionary",
            keyword_length = 3,
        },
    }),
})

cmp.setup.cmdline({ "/", "?" }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
        { name = "buffer" },
    },
})

cmp.setup.cmdline(":", {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
        { name = "path" },
    }, {
        { name = "cmdline" },
    }),
})

-- dictionary completion source
dict.setup({
    exact = 2,
    first_case_insensitive = true,
    document = false,
    document_command = "wn %s -over",
    async = false,
    max_items = 1048,
    capacity = 5,
    debug = false,
})

dict.switcher({
    spelllang = {
        en = "/home/kalankaboom/.config/nvim/dicts/en_GB.dict",
        fr = "/home/kalankaboom/.config/nvim/dicts/fr_CH.dict",
    },
})

-- trouble
trouble.setup({
    position = "bottom",
    height = 10,
    width = 50,
    icons = false,
    mode = "workspace_diagnostics",
    fold_open = "",
    fold_closed = "",
    group = true,
    padding = false,
    action_keys = {
        close = "q",
        cancel = "<esc>",
        refresh = "r",
        jump = { "<cr>", "<tab>" },
        open_split = { "<c-h>" },
        open_vsplit = { "<c-l>" },
        open_tab = { "<c-t>" },
        jump_close = { "o" },
        toggle_mode = "m",
        toggle_preview = "P",
        hover = "K",
        preview = "p",
        close_folds = { "zM", "zm" },
        open_folds = { "zR", "zr" },
        toggle_fold = { "zA", "za" },
        previous = "k",
        next = "j",
    },
    indent_lines = true,
    auto_open = false,
    auto_close = false,
    auto_preview = true,
    auto_fold = true,
    auto_jump = { "lsp_definitions" },
    use_diagnostic_signs = true,
})

-- lualine
local lualine_a = {
    {
        "filename",
        file_status = true,
        newfile_status = true,
        symbols = {
            modified = "[]",
            readonly = "[󰛤]",
            unnamed = "[]",
            newfile = "[]",
        },
    },
}

local lualine_b = { { "branch" } }

lualine.setup({
    options = {
        icons_enabled = false,
        theme = "kalankaboom",
        component_separators = { left = "", right = "" },
        section_separators = { left = "", right = "" },
        always_divide_middle = true,
        globalstatus = true,
        refresh = {
            statusline = 500,
            tabline = 10000,
            winbar = 10000,
        },
    },
    sections = {
        lualine_a = lualine_a,
        lualine_b = lualine_b,
        lualine_c = {
            {
                "diagnostics",
                symbols = {
                    error = " ",
                    warn = " ",
                    info = " ",
                    hint = "󰌶 ",
                },
            },
        },
        lualine_x = { "" },
        lualine_y = { "location" },
        lualine_z = { "progress" },
    },
    inactive_sections = {
        lualine_a = lualine_a,
        lualine_b = lualine_b,
        lualine_c = {
            {
                "diagnostics",
                colored = false,
                symbols = {
                    error = " ",
                    warn = " ",
                    info = " ",
                    hint = "󰌶 ",
                },
            },
        },
        lualine_x = {},
        lualine_y = { "location" },
        lualine_z = { "progress" },
    },
})

-- formatter
local prettier = function()
    return {
        exe = "prettier",
        args = {
            "--stdin-filepath",
            util.escape_path(util.get_current_buffer_file_path()),
            "--tab-width",
            glob_shift_width,
            "--prose-wrap always",
        },
        stdin = true,
        -- try_node_modules = true,
    }
end

local clangformat = function()
    return {
        exe = "clang-format",
        args = {
            "-style=file:$HOME/.config/.clang-format",
            "-assume-filename",
            util.escape_path(util.get_current_buffer_file_name()),
        },
        stdin = true,
        try_node_modules = true,
    }
end

formatter.setup({
    logging = true,
    log_level = vim.log.levels.WARN,
    filetype = {
        cpp = { clangformat },
        c = { clangformat },
        javascript = { prettier },
        typescript = { prettier },
        css = { prettier },
        html = { prettier },
        yaml = { prettier },
        markdown = { prettier },
        json = { prettier },
        python = {
            function()
                return {
                    exe = "black",
                    args = {
                        "-q",
                        "-",
                    },
                    stdin = true,
                }
            end,
        },
        lua = {
            function()
                return {
                    exe = "stylua",
                    args = {
                        "-f",
                        "~/.config/stylua/stylua.toml",
                        "--stdin-filepath",
                        util.escape_path(util.get_current_buffer_file_path()),
                        "--",
                        "-",
                    },
                    stdin = true,
                }
            end,
        },
        sh = {
            function()
                return {
                    exe = "shfmt",
                    args = { "-i", glob_shift_width, "-ci" },
                    stdin = true,
                }
            end,
        },
        ["*"] = {
            require("formatter.filetypes.any").remove_trailing_whitespace,
            -- function()
            --     return {
            --         exe = "sed",
            --         args = {
            --             util.quote_cmd_arg(
            --                 util.wrap_sed_replace(
            --                     "\t",
            --                     string.rep(" ", vim.opt.shiftwidth:get()),
            --                     "g"
            --                 )
            --             ),
            --         },
            --         stdin = true,
            --     }
            -- end,
        },
    },
})
