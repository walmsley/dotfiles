local colors = require("kalankaboom.colours")
local kalankaboom = {}

kalankaboom.normal = {
    a = { bg = colors.Magenta, fg = colors.Black, gui = "bold" },
    b = { bg = colors.DarkMagenta, fg = colors.Black },
    c = { bg = "NONE", fg = colors.White },
}

kalankaboom.insert = {
    a = kalankaboom.normal.a,
    b = kalankaboom.normal.b,
}

kalankaboom.visual = {
    a = kalankaboom.normal.a,
    b = kalankaboom.normal.b,
}

kalankaboom.replace = {
    a = kalankaboom.normal.a,
    b = kalankaboom.normal.b,
}

kalankaboom.command = {
    a = kalankaboom.normal.a,
    b = kalankaboom.normal.b,
}

kalankaboom.inactive = {
    a = { bg = "NONE", fg = colors.DarkMagenta, gui = "underline" },
    b = { bg = "NONE", fg = colors.DarkMagenta, gui = "underline" },
    c = { bg = "NONE", fg = colors.DarkMagenta, gui = "underline" },
}

return kalankaboom
