local kalankaboom = {}

local function set_highlights(group, cols)
    vim.api.nvim_command(
        string.format(
            "highlight %s gui=%s guifg=%s guibg=%s guisp=%s",
            group,
            cols.style or "NONE",
            cols.fg or "NONE",
            cols.bg or "NONE",
            cols.sp or "NONE"
        )
    )
end

local function load_colorscheme(scheme)
    vim.api.nvim_command("highlight clear")
    if vim.fn.exists("sintax_on") then vim.api.nvim_command("syntax reset") end

    vim.opt.termguicolors = true
    vim.g.colors_name = "kalankaboom"

    if type(overrides) == "table" then
        if next(overrides) ~= nil then
            scheme = vim.tbl_deep_extend("force", {}, scheme, overrides)
        end
    end

    for grp, col in pairs(scheme) do
        set_highlights(grp, col)
    end
end

function kalankaboom.setup()
    local colours = require("kalankaboom.colours")
    local scheme = require("kalankaboom.scheme").load_colors(colours)

    load_colorscheme(scheme)
end

return kalankaboom
