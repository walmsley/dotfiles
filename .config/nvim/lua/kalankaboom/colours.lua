local colours = {}

colours.background = "#000000"
colours.Red        = "#FF0000"
colours.Blue       = "#5555FF" ---
colours.Green      = "#00FF00"
colours.Purple     = "#660066" ---
colours.Yellow     = "#FFFF00"
colours.Orange     = "#FFA500" ---
colours.Violet     = "#9400D0" ---
colours.Magenta    = "#FF00FF"
colours.Pink       = "#FF1490" ---
colours.White      = "#FFFFFF"
colours.Cyan       = "#00FFFF"
colours.Aqua       = "#0FFFF0" ---
colours.Black      = "#000000"
colours.Grey       = "#555555"
colours.LightGrey  = "#999999" ---
colours.Custom_1   = "#222233" --
colours.Custom_2   = "#AFFDF1" --
colours.Custom_3   = "#E2E7E6" --

colours.DarkRed     = "#990000"
colours.DarkOrange  = "#FF8C00" ---
colours.DarkBlue    = "#0000AA" ---
colours.DarkPurple  = "#330033" ----
colours.DarkGreen   = "#009900"
colours.DarkYellow  = "#999900"
colours.DarkMagenta = "#990099"
colours.DarkCyan    = "#009999"
colours.DarkGrey    = "#333333" ---
colours.DarkGrey_2  = "#220022" --

return colours
