call plug#begin('~/.config/nvim/my-vim-plugins')
Plug 'neovim/nvim-lspconfig'

Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

Plug 'L3MON4D3/LuaSnip'
Plug 'saadparwaiz1/cmp_luasnip'
Plug 'rafamadriz/friendly-snippets'

Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' }
Plug 'nvim-treesitter/nvim-treesitter-context'

Plug 'windwp/nvim-autopairs'
Plug 'windwp/nvim-ts-autotag'

Plug 'JoosepAlviste/nvim-ts-context-commentstring'
Plug 'numToStr/Comment.nvim'

Plug 'mhinz/vim-startify'

Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'

Plug 'folke/trouble.nvim'
Plug 'mbbill/undotree'
Plug 'tpope/vim-fugitive'

Plug 'luukvbaal/nnn.nvim'

Plug 'nvim-treesitter/playground'

Plug 'uga-rosa/cmp-dictionary'

Plug 'mhartington/formatter.nvim'
" Plug 'mfussenegger/nvim-lint'

Plug 'godlygeek/tabular'

Plug 'nvim-lualine/lualine.nvim'
Plug 'xiyaowong/virtcolumn.nvim'

Plug 'norcalli/nvim-colorizer.lua'

" Plug 'gennaro-tedesco/nvim-peekup'
call plug#end()

    " undotreee
let g:undotree_ShortIndicators = 1
let g:undotree_TreeNodeShape = ''
let g:undotree_TreeVertShape = '│'
let g:undotree_TreeSplitShape = '╱'
let g:undotree_TreeReturnShape = '╲'
let g:undotree_HelpLine = 0

    " vim-startify
let g:startify_lists = [
    \ { 'type': 'files',     'header': ['   Freshest  '] },
    \ { 'type': 'sessions',  'header': ['   Sessions  󰌢'] },
\ ]
let g:startify_enable_special = 0
let g:startify_padding_left = 5
let g:startify_files_number = 20


let s:mushroom = [
\ "",
\ "",
\ "                          .-'~~~-.                ",
\ "                        .'o  oOOOo`.                ",
\ "                       :~~~-.oOo   o`.            ",
\ "                        `. \\ ~-.  oOOo.            ",
\ "                          `.; / ~.  OO:            ",
\ "                          .'  ;-- `.o.'            ",
\ "                         ,'  ; ~~--'~            ",
\ "                         ;  ;                    ",
\ '   _______\|/__________\\;_\\//___\|/________    ',
\ "",
\ "",
\ ]

let s:diver = [
\ "",
\ "",
\ "                             o                            ",
\ "                            o  o                            ",
\ "                            o o o                        ",
\ "                          o                                ",
\ "         .              o    ______          ______        ",
\ "   \\_____)\\_____        _ *o(_||___)________/___            ",
\ "   /--v____ __`<      O(_)(       o  ______/    \\        ",
\ "           )/        > ^  `/------o-'            \\        ",
\ "           '       D|_|___/                                ",
\ "",
\ "",
\ ]

let s:micro = [
\ "",
\ '                                        __            ',
\ '                                        ||            ',
\ '                                       ====            ',
\ '                                       |  |__        ',
\ '   -. .-.   .-. .-.   .-. .-.   .      |  |-.\        ',
\ '   ||\|||\ /|||\|||\ /|||\|||\ /|      |__|  \\        ',
\ '   |/ \|||\|||/ \|||\|||/ \|||\||       ||   ||        ',
\ '   ~   `-~ `-`   `-~ `-`   `-~ `-     ======__|        ',
\ '                                     ________||__    ',
\ '                                    /____________\    ',
\ "",
\ "",
\ ]

let s:mountains = [
\ "",
\ "",
\ "",
\ '           _    .  ,   .           .                            ',
\ "       *  / \\_ *  / \\_      _  *        *   /\\'__        *    ",
\ "         /    \\  /    \\,   ((        .    _/  /  \\  *'.        ",
\ "    .   /\\/\\  /\\/ :' __ \\_  `          _^/  ^/    `--.        ",
\ "       /    \\/  \\  _/  \\-'\\      *    /.' ^_   \\_   .'\\  *    ",
\ '     /\  .-   `. \/     \ /==~=-=~=-=-;.  _/ \ -. `_/   \    ',
\ "    /  `-.__ ^   / .-'.--\\ =-=~_=-=~=^/  _ `--./ .-'  `-        ",
\ "   /        `.  / /       `.~-^=-=~=^=.-'      '-._ `._        ",
\ "",
\ "",
\ ]

let s:lonebeach = [
\ "             ___   ____                                        ",
\ "           /' --;^/ ,-_\\     \\ | /                            ",
\ "          / / --o\\ o-\\ \\\\   --(_)--                            ",
\ "         /-/-/|o|-|\\-\\\\|\\\\   / | \\                            ",
\ "          '`  ` |-|   `` '                                    ",
\ "                |-|                                            ",
\ "                |-|O                                            ",
\ "                |-(\\,__                                        ",
\ "             ...|-|\\--,\\_....                                ",
\ "         ,;;;;;;;;;;;;;;;;;;;;;;;;,.                            ",
\ "   ~~,;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;~~~~~~~~~~~~~~~~~~~~~~~    ",
\ "   ~;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,  ______   -------   ",
\ "",
\ ]

let s:desert = [
\ "              ,,                               .-.            ",
\ "             || |                               ) )            ",
\ "             || |   ,                          '-'            ",
\ "             || |  | |                                        ",
\ "             || '--' |                                        ",
\ "       ,,    || .----'                                        ",
\ "      || |   || |                                            ",
\ "      |  '---'| |                                            ",
\ "      '------.| |                                  _____        ",
\ '      ((_))  || |      (  _                       / /|\ \    ',
\ "      (o o)  || |      ))(\"),                    | | | | |    ",
\ '   ____\_/___||_|_____((__^_))____________________\_\|/_/__    ',
\ "",
\ ]

let s:taj = [
\ '                          !                            ',
\ '                         /^\                            ',
\ '                       /     \                        ',
\ '    |               | (       ) |               |    ',
\ '   /^\  |          /^\ \     / /^\          |  /^\    ',
\ '   |O| /^\        (   )|-----|(   )        /^\ |O|    ',
\ '   |_| |-|    |^-^|---||-----||---|^-^|    |-| |_|    ',
\ '   |O| |O|    |/^\|/^\||  |  ||/^\|/^\|    |O| |O|    ',
\ '   |-| |-|    ||_|||_||| /^\ |||_|||_||    |-| |-|    ',
\ '   |O| |O|    |/^\|/^\||(   )||/^\|/^\|    |O| |O|    ',
\ '   |-| |-|    ||_|||_||||   ||||_|||_||    |-| |-|    ',
\ '   |O| |_|----|___|___|||___|||___|___|----|_| |O|    ',
\ '   |_|                                         |_|    ',
\ ]

let g:asciibanners = [
\    s:mushroom,
\    s:diver,
\    s:micro,
\    s:mountains,
\    s:lonebeach,
\    s:desert,
\    s:taj,
\]

let g:dayarray = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

let g:startify_custom_header = g:asciibanners[index(g:dayarray, strftime("%a"))]


" let g:mkdp_refresh_slow = 1
" let g:mkdp_open_to_the_world = 0
" let g:mkdp_open_ip = ''
" let g:mkdp_browserfunc = ''
"
" let g:mkdp_preview_options = {
"     \ 'mkit': {},
"     \ 'katex': {},
"     \ 'uml': {},
"     \ 'maid': {},
"     \ 'disable_sync_scroll': 0,
"     \ 'sync_scroll_type': 'relative',
"     \ 'hide_yaml_meta': 1,
"     \ 'sequence_diagrams': {},
"     \ 'flowchart_diagrams': {},
"     \ 'content_editable': v:false,
"     \ 'disable_filename': 0,
"     \ 'toc': {}
"     \ }
"
" let g:mkdp_markdown_css = ''
" let g:mkdp_highlight_css = ''
" let g:mkdp_port = ''
" let g:mkdp_page_title = '[${name}]'
" let g:mkdp_filetypes = ['markdown']
" let g:mkdp_theme = 'dark'

function MyTabLabel(n)
    const buflist = tabpagebuflist(a:n)
    const winnr = tabpagewinnr(a:n)
    return fnamemodify(bufname(buflist[winnr - 1]), ':t')
endfunction

function MyTabLine()
    let s = ''
    for i in range(tabpagenr('$'))
        " select the highlighting
        if i + 1 == tabpagenr()
            let s ..= '%#TabLineSel#'
        else
            let s ..= '%#TabLine#'
        endif

        " set the tab page number (for mouse clicks)
        let s ..= '%' .. (i + 1) .. 'T'

        " the label is made by MyTabLabel()
        let s ..= ' %{MyTabLabel(' .. (i + 1) .. ')} '
    endfor

    " after the last tab fill with TabLineFill and reset tab page nr
    let s ..= '%#TabLineFill#%T'

    " right-align the label to close the current tab page
    " if tabpagenr('$') > 1
    "     let s ..= '%=%#TabLine#%999Xclose'
    " endif

    return s
endfunction

set tabline=%!MyTabLine()

lua << EOF
    require("init")
EOF
